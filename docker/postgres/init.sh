#!/bin/bash

set -e

# Short Form, since it's quite a bit to type
psqlexecADM="psql --username $POSTGRES_USER --dbname $POSTGRES_DB -c"

# Discord Database Set-Up
$psqlexecADM "CREATE DATABASE $DISCORD_BOT_DB;"
$psqlexecADM "GRANT ALL PRIVILEGES ON DATABASE $DISCORD_BOT_DB TO $POSTGRES_USER;"

# Grafana Database Set-Up
$psqlexecADM "CREATE USER $GRAFANA_USER WITH PASSWORD '$GRAFANA_PASSWORD';"
$psqlexecADM "CREATE DATABASE $GRAFANA_ANNOTATIONS_DB;"
$psqlexecADM "GRANT CONNECT ON DATABASE $GRAFANA_ANNOTATIONS_DB TO $GRAFANA_USER;"