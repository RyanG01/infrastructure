environment               = "prod"
os                        = "unix"
domain                    = "example.com"
company_name              = "my-company"
company_prefix            = "company"
mail                      = "admin@localhost"
infrastructure_maintainer = "Myself"

# Database Variables
database_user     = "pgadmin"
database_password = "password"


# Discord Bot Variables
discord_bot_client_id = "000000000000000000"
discord_bot_dev_guild = "000000000000000000"
discord_bot_token     = "000000000000000000000000.000000.000000000000000000000000000"


# Monitoring Variables
grafana_admin_user     = "admin"
grafana_admin_password = "password"
grafana_subdomain      = "grafana"
gitlab_client_id       = "0000000000000000000000000000000000000000000000000000000000000000"
gitlab_client_secret   = "0000000000000000000000000000000000000000000000000000000000000000"
gitlab_group           = "company-gitlab"

# Miscellaneous Variables
discord_invite_url = "https://discord.gg/000000000000000000"
cloudflare_api_key = "000000000000000000000000000000000000000000000000"