locals {
  prod_only = var.environment == "prod" ? "true" : "false"
  gf_provisioning = "/etc/grafana/provisioning"
  
  gf_mounts = [
    { name = "custom.ini", path = "/etc/grafana/grafana.ini" },
    { name = "provisioning", path = local.gf_provisioning },
    { name = "dashboards", path = "/etc/dashboards" },
  ]

  gf_init = [
    "ADMIN_USER=${var.grafana_admin_user}",
    "ADMIN_PASSWORD=${var.grafana_admin_password}",
    "SECRET_KEY=${var.grafana_secret_key}",
    "PROTOCOL=${var.environment == "prod" ? "https" : "http"}",
    "SUBDOMAIN=${var.grafana_subdomain}",
    "DOMAIN=${var.domain}",
  ]

  gf_oauth2 = [
    "GITLAB_ENABLED=${local.prod_only}",
    "GITLAB_CLIENT_ID=${var.gitlab_client_id}",
    "GITLAB_CLIENT_SECRET=${var.gitlab_client_secret}",
    "GITLAB_GROUP=${var.gitlab_group}",
  ]

  gf_datasource = [
    "PROVISIONING=${local.gf_provisioning}",
    "LOKI_HOST=${var.company_prefix}-loki",
    "PROM_HOST=${var.company_prefix}-prometheus",
    "PG_HOST=${var.company_prefix}-postgres",
    "PG_GF_USER=${var.company_prefix}_grafana",
    "PG_GF_PASS=${var.grafana_admin_password}",
    "PG_GF_DB=${var.company_prefix}_grafana_db"
  ]
}

resource "docker_volume" "loki_data" {
  name = "${var.company_prefix}-loki-data"
}

resource "docker_volume" "grafana_data" {
  name = "${var.company_prefix}-grafana-data"
}

resource "docker_volume" "prometheus_data" {
  name = "${var.company_prefix}-prometheus-data"
}

resource "docker_image" "loki" {
  name = "${var.company_name}/loki"
  build {
    path = "docker/loki/"
    tag  = ["${var.company_name}/loki:1.0.0"]
    label = {
      "author" = var.infrastructure_maintainer
    }
  }
}

resource "docker_image" "grafana" {
  name = "${var.company_name}/grafana"
  build {
    path = "docker/grafana/"
    tag  = ["${var.company_name}/grafana:1.0.0"]
    label = {
      "author" = var.infrastructure_maintainer
    }
  }
}

resource "docker_image" "prometheus" {
  name = "${var.company_name}/prometheus"
  build {
    path = "docker/prometheus/"
    tag  = ["${var.company_name}/prometheus:1.0.0"]
    label = {
      "author" = var.infrastructure_maintainer
    }
  }
}

resource "docker_container" "loki" {
  image = docker_image.loki.latest
  name  = "${var.company_prefix}-loki"

  volumes {
    volume_name    = docker_volume.loki_data.name
    container_path = "/loki"
  }

  networks_advanced {
    name = docker_network.internal.name
  }
}

resource "docker_container" "prometheus" {
  image = docker_image.prometheus.latest
  name  = "${var.company_prefix}-prometheus"

  volumes {
    volume_name    = docker_volume.prometheus_data.name
    container_path = "/prometheus"
  }

  env = [
    "DISCORD_BOT_JOB=${var.company_prefix}-bot",
    "DISCORD_BOT_METRICS=${var.company_prefix}-bot:${var.metrics_port}"
  ]

  networks_advanced {
    name = docker_network.internal.name
  }
}

resource "docker_container" "grafana" {
  image = docker_image.grafana.latest
  name  = "${var.company_prefix}-grafana"

  env = concat(local.gf_init, local.gf_oauth2, local.gf_datasource)

  volumes {
    volume_name    = docker_volume.grafana_data.name
    container_path = "/var/lib/grafana"
  }

  networks_advanced {
    name = docker_network.internal.name
  }

  dynamic "mounts" {
    for_each = local.gf_mounts
    content {
      type = "bind"
      source = "${abspath(path.root)}/docker/grafana/${mounts.value.name}"
      target = mounts.value.path
    }
  }

  depends_on = [
    docker_container.loki
  ]
}