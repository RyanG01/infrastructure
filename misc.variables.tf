variable "environment" {
  type        = string
  description = "Deployment Environment"

  default = "develop"

  validation {
    condition     = contains(["develop", "prod"], var.environment)
    error_message = "The environment must be either production (prod) or development (develop)."
  }
}

variable "os" {
  type        = string
  description = "Operating System"
  default     = "win"

  validation {
    condition     = contains(["win", "unix"], var.os)
    error_message = "The Operating System must be either Windows (win) or Linux/Mac Os (unix)."
  }
}

variable "domain" {
  type        = string
  description = "Domain Name"
  default     = "localhost"
}

variable "company_prefix" {
  type        = string
  description = "Company prefix"
  default     = "company"
}

variable "company_name" {
  type        = string
  description = "Company name in kebab-case"
  default     = "company"
}

variable "infrastructure_maintainer" {
  type        = string
  description = "Infrastructure Maintainer"
  default     = "Roman Shchekotov"
}

variable "mail" {
  type        = string
  description = "Mail Address"
  default     = "admin@localhost"
}

variable "discord_invite_url" {
  type        = string
  description = "Discord Invite URL"
  default     = "https://discord.gg/0000000000"
}

variable "cloudflare_api_key" {
  type        = string
  description = "Cloudflare API Key"
  sensitive   = true
  default     = "0000000000000000000000000000000000000000"

  validation {
    condition     = can(regex("^[A-Za-z\\d_-]{40}$", var.cloudflare_api_key))
    error_message = "Invalid Cloudflare API Token was specified!"
  }
}